package optimization;

public class GuiUpdater extends Thread{
     private static final long SLEEP_TIME =60;
     
     private Optimization callback;
     
     public GuiUpdater(Optimization callback) {
         super();
         this.callback = callback;
     }
     
      @Override public void run() {
        //Run as long as Algorithm is not in FINISHED state
        while(true) {

            try{
                callback.Information.setText(callback.alg.getInformation());
                callback.myCanvas1.setData(callback.alg.coordinates);
            }catch(Exception e) {}
            
            //Save CPU time by sleeping when idle.
            try {
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException ex) {
                System.out.println("SLEEP FAIL!");
            }   
        }

    }
     
     
     
}
