package optimization;

import java.awt.*;
import java.util.ArrayList;
import optimization.Algorithms.helper.Coordinates;

/**
 * This is the canvas that displays the plate and all the circles.
 * @author Alexander von Birgelen
 */
public class MyCanvas extends Canvas {
    
    public double x; //mm
    public double y; //mm
    public double d; //mm
    
    private int xPixel = 0; //px
    private int yPixel = 0; //px
    private int dPixel = 0; //px
    
    double pixelPerMm; //px
    
    //Data to draw:
    private ArrayList<Coordinates> data;
    
    /**
     * Set the Data in mm for x and y of the plate and the diameter of the circles
     * @param x
     * @param y
     * @param d
     * @return 
     */
    public boolean setConfigurationData(double x, double y, double d) { //Data in mm
        
        this.x = x;
        this.y = y;
        this.d = d;
        
        this.calculateProportion();
        
        this.repaint();
        
        return true;
    }
    
    /**
     * This is called from the class Algorithm to set Data of the circles.
     * @param data ArrayList containing Coordinates
     * @return 
     */
    public boolean setData(ArrayList<Coordinates> data) {
        this.data = data;
        this.repaint();
        return true;
    }
    
    /**
     * Calculate proportions to scale x,y and d millimeter variables 
     * to display in correct proportions on the window.
     */
    private void calculateProportion() {
        
        if (x > y ) {
           pixelPerMm = ((double)this.getWidth()-1.0) / x;
        }else{
           pixelPerMm = ((double)this.getHeight()-1.0) / y;
        }
        
        xPixel = (int) (x * pixelPerMm);
        yPixel = (int) (y * pixelPerMm);
        dPixel = (int) (d * pixelPerMm);
        
        System.out.println("xPixel: "+xPixel+"\nyPixel: "+yPixel+"\ndPixel: "+dPixel+"\nPixel per mm: "+pixelPerMm);
        
    }
    
    /**
     * The paint method. This draws the rectangle and all circles.
     * @param g 
     */
    @Override public void paint(final Graphics g) {
        super.paint(g);
    
        //Draw Rectangular plate:
        g.drawRect(0, 0, xPixel, yPixel);
        
        try {
            //Draw all spheres:
            for(Coordinates c : data) {
                //Coordinates are mid Circle!!!!
                //Java needs upper left corner!!!
                //So substract radius from x and y for painting
                
                int cx = (int)( (c.x-(d/2.0)) * pixelPerMm);
                int cy = (int)( (c.y-(d/2.0)) * pixelPerMm);
                
                g.fillOval(cx, cy, dPixel, dPixel);
            }
        }catch(Exception e) {
            System.out.println("No Circles to plot.");
        }
  }
    
}
