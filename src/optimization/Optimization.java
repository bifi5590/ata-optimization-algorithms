package optimization;

import optimization.Algorithms.*;

/**
 *Program for testing different Optimization Algorithms.
 * 
 * This GUI handles Algorithm selection and start/stop/pause functions.
 * 
 * @author Alexander von Birgelen
 * @version 1.2
 */
public class Optimization extends javax.swing.JFrame {
    
    public Algorithm alg;
    private GuiUpdater updater;

    /**
     * Creates new form GUI
     */
    public Optimization() {
        initComponents();
        //myCanvas1.setConfigurationData(x, y, d);
        myCanvas1.setConfigurationData(myCanvas1.getWidth(), myCanvas1.getHeight(), myCanvas1.getWidth()/10.0);
        
        pauseButton.setEnabled(false);
        runButton.setEnabled(true);
        stepButton.setEnabled(true);
        resetButton.setEnabled(true);
        selectedAlgorithm.setEnabled(true);
        
        
        updater = new GuiUpdater(this);
        updater.start();
        
    }    

    /**
     * This method is used to select an algorithm.
     * @param i 0=Gradient, 1=Hillclimbing, 2=Taboo, 3=simulated annealing, 4= great deluge
     * @return Returns selected Algorithm
     */
    private Algorithm initAlgorithm(int i) {
        
        switch(i) {
            case 0:
                alg = new GradientMethod(this);
                break;
            case 1:
                alg = new Hillclimbing(this);
                break;
            case 2:
                alg = new TabooSearch(this);
                break;
            case 3:
                alg = new SimulatedAnnealing(this);
                break;
            case 4:
                alg = new GreatDeluge(this);
                break;
            default:
                alg = null;
                break;
        }
        
        this.Information.setText(alg.getInformation());
        
        return alg;
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        selectedAlgorithm = new javax.swing.JComboBox();
        runButton = new javax.swing.JButton();
        pauseButton = new javax.swing.JButton();
        stepButton = new javax.swing.JButton();
        resetButton = new javax.swing.JButton();
        myCanvas1 = new optimization.MyCanvas();
        jScrollPane1 = new javax.swing.JScrollPane();
        Information = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Optimization Algorithm testing");
        setResizable(false);

        selectedAlgorithm.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Gradient", "Hillclimbing", "Taboo", "Simulated Annealing", "Great Deluge" }));

        runButton.setText("Run");
        runButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runButtonActionPerformed(evt);
            }
        });

        pauseButton.setText("Pause");
        pauseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pauseButtonActionPerformed(evt);
            }
        });

        stepButton.setText("Step");
        stepButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stepButtonActionPerformed(evt);
            }
        });

        resetButton.setText("Reset");
        resetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetButtonActionPerformed(evt);
            }
        });

        Information.setColumns(20);
        Information.setRows(5);
        Information.setText("Select an algorithm and press run!");
        jScrollPane1.setViewportView(Information);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(myCanvas1, javax.swing.GroupLayout.PREFERRED_SIZE, 401, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(runButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pauseButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(stepButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(resetButton)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 84, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(selectedAlgorithm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(runButton)
                    .addComponent(pauseButton)
                    .addComponent(stepButton)
                    .addComponent(resetButton)
                    .addComponent(selectedAlgorithm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(myCanvas1, javax.swing.GroupLayout.PREFERRED_SIZE, 401, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(95, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Event for the run button.
     * Starts the Algorithm (which is a thread)
     * @param evt 
     */
    private void runButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_runButtonActionPerformed
        int index  = this.selectedAlgorithm.getSelectedIndex();
        //Init algorithm if necessary
        if(alg == null) {
            this.initAlgorithm(index);
        }
        //Start the Thread;
        alg.start();
        
        pauseButton.setEnabled(true);
        stepButton.setEnabled(false);
        runButton.setEnabled(false);
        selectedAlgorithm.setEnabled(false);
    }//GEN-LAST:event_runButtonActionPerformed

    /**
     * Reset the Algorithm.
     * @param evt 
     */
    private void resetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetButtonActionPerformed
        
        //Try to end Algorithm and clear the canvas.
        try{
            alg.end();
            myCanvas1.setData(null);
        }catch(Exception e) {}
        alg = null;
        Information.setText("");
        
        runButton.setEnabled(true);
        stepButton.setEnabled(false);
        pauseButton.setEnabled(false);
        selectedAlgorithm.setEnabled(true);
    }//GEN-LAST:event_resetButtonActionPerformed

    /**
     * Pauses the Algorithm.
     * Thread remains in run mode but changes state internally.
     * @param evt 
     */
    private void pauseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pauseButtonActionPerformed
        try{
            alg.end();
            System.out.println("Algorithm stopped.");
            runButton.setEnabled(true);
            pauseButton.setEnabled(false);
            stepButton.setEnabled(true);
            selectedAlgorithm.setEnabled(false);
            //alg.triggerRepaint();
        }catch(Exception e) {}
    }//GEN-LAST:event_pauseButtonActionPerformed

    /**
     * Perform one step of Algorithm.
     * @param evt 
     */
    private void stepButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stepButtonActionPerformed
        try {
            //Init if necessary
            if(alg == null) {
                this.initAlgorithm(this.selectedAlgorithm.getSelectedIndex());
            }
            //Perform one step
            alg.doStep();
            System.out.println("One step forward");
            //Write data to Gui
            //alg.triggerRepaint();
        }catch(Exception e) {}
    }//GEN-LAST:event_stepButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Optimization.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Optimization.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Optimization.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Optimization.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Optimization().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JTextArea Information;
    private javax.swing.JScrollPane jScrollPane1;
    public optimization.MyCanvas myCanvas1;
    public javax.swing.JButton pauseButton;
    private javax.swing.JButton resetButton;
    public javax.swing.JButton runButton;
    private javax.swing.JComboBox selectedAlgorithm;
    public javax.swing.JButton stepButton;
    // End of variables declaration//GEN-END:variables
}
