package optimization;

import optimization.Algorithms.helper.*;
import java.util.ArrayList;

/**
 * This is the main class for the Algorithms.
 * This extends the class Thread.
 * All Algorithms must extend this class and implement the abstract method
 * work().
 *
 * @author Alexander von Birgelen
 */
public abstract class Algorithm extends Thread {
    
    private static final long SLEEP_TIME = 100;
    
    public ArrayList<Coordinates> coordinates = new ArrayList<>();
    
    private String name;
    protected String status = ""; //append all info to this variable
    protected String description = "";
    
    private Optimization callback; //Callback to trigger repaint of gui
    
    protected AlgState state; //state of algorithm
    protected long steps;
    
    /**
     * Initialize Algorithm.
     * 
     * @param callback This is a reference to the GUI to trigger updates and transmit data.
     */
    public Algorithm(Optimization callback) {
        super();
        this.callback = callback;
        name = this.getClass().getSimpleName();
        state = AlgState.INIT;
        steps = 0;
    }
    
    /**
     * Override run Method of Algorithm
     */
    @Override public void run() {
        //Run as long as Algorithm is not in FINISHED state
        while(state != AlgState.FINISHED) {
            
            //Call work and triggerRepaint as fast as possible to solve
            //the Algorithm quickly:
            while(state == AlgState.RUNNING) {
                work();
                //triggerRepaint();
            }

            //Save CPU time by sleeping when idle.
            try {
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException ex) {
                System.out.println("SLEEP FAIL!");
            }   
        }
        
        this.status = this.status + "\nFinished.";
        
        callback.pauseButton.setEnabled(false);
        callback.runButton.setEnabled(false);
        callback.stepButton.setEnabled(false);
        
        
    }
    
    /**
     * Start the Thread. Sets internal State to RUNNING.
     */
    @Override public void start() {
        state = AlgState.RUNNING;
        try {
            super.start();
        }catch(Exception e){}
        status += "Algorithm started.";
        System.out.println("Algorithm started.");
    }
    
    /**
     * Switch internal state to PAUSED. Thread however is still running.
     */
    public void end() {
        state = AlgState.PAUSED;    
        System.out.println("Stopping Algorithm after current step.");
    }
    
    /**
     * Set information of current progress in GUI.
     */
    /*public void triggerRepaint() {
        callback.Information.setText(getInformation());
        try {
            callback.myCanvas1.setData(coordinates);
        }catch(Exception e){
            System.out.println("Unable to trigger GUI repaint from Algorithm.");
        }

    }*/

    /**
     * Perform one step!
     */
    public void doStep() {
        
        //Call end if Algorithm is in state RUNNING.
        if(state == AlgState.RUNNING) {
            this.end();
        }else{
            //Call work method when state is not finished
            if(state != AlgState.FINISHED) {
                work();
            }
        }
        
        //Repaint
        //triggerRepaint();
        
        state = AlgState.STEP;
        
    }
    
    /**
     * work method. This method must be implemented by every Algorithm.
     * The Algorithm must carry out one step only in this method.
     * @return 
     */
    public abstract boolean work();
    
    /**
     * Generate information String.
     * @return 
     */
    public String getInformation() {
        return name+"\n\n"
               +"Steps: "+steps+"\n"
               +"Circles on plate: "+coordinates.size()+"\n"
               +status;
    }
    
}
