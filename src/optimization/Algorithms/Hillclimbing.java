
package optimization.Algorithms;

import java.util.Random;
import optimization.Algorithm;
import optimization.Algorithms.helper.AlgState;
import optimization.Algorithms.helper.Coordinates;
import optimization.Optimization;

public class Hillclimbing extends Algorithm {
    
    //Diameter of one circle
    private double d;
    //X size:
    private double x;
    //Y size;
    private double y;
    
    //help variables used in Algorithm:
    private boolean opt, setgen;
    private int stepx, stepy,c, xc, yc;
    private double dist;
    private boolean move = false;
    
    //This stores the step when a new circle is added
    private long lastCircleAddedStep = 0;
    //Number of maximum tries to generate a random circle:
    private int maxtries = 1500;
    //The threshold to stop algorithm when no improvement in circle count was made for some steps:
    private int tryThreshold = 10000;
    //The random number generator:
    private Random rand;

    /**
     * Hillclimbing Algorithm
     * @param callback 
     */
    public Hillclimbing(Optimization callback) {
        super(callback);
        
        y = callback.myCanvas1.getHeight();
        x = callback.myCanvas1.getWidth();
        d = callback.myCanvas1.getWidth()/10.0;
        
        //Init random generator
        rand = new Random();
 
    }

    @Override
    public boolean work() {
        
        steps++;
        //Try and minimize the distance between circles:
        opt = optimizeSetStep();
        //Try to create additional circles:
        setgen = generateSet();
        
        //Note last found optimization:
        if(setgen) {
            lastCircleAddedStep = steps;
        }
        
        //Stop Algorithm if no optimization was found for some time:
        if(steps -lastCircleAddedStep > tryThreshold) {
            this.state = AlgState.FINISHED;
        }

        return true;
    }       
    
    /**
     * Check if a circle with the given coordinates is overlapping one of the other circles
     * @param nx
     * @param ny
     * @param index
     * @return 
     */
    private boolean checkOverlap(int nx, int ny, int index) {
        
        //Move through all coordinates:
        for(int i=0;i<coordinates.size();i++) {
            //Do not check the moved circle
            if(i != index) {
                Coordinates c = coordinates.get(i);
                dist =(int) Math.pow(Math.abs(nx-c.x), 2) + Math.pow( Math.abs(ny-c.y), 2);

                if(dist < d*d) {
                    //Overlap found.
                    return true;
                }
            }

        }
        
        return false;
    }
    
    /**
     * Try to generate some circles!
     * @return 
     */
    private boolean generateSet() {
        
        //Try a couple of times to generate a circle:
        for(int i=0; i<maxtries;i++) {
            //Generate random circle:
            int nx = rand.nextInt((int)(x-d))+(int)(d/2);
            int ny = rand.nextInt((int)(y-d))+(int)(d/2);
            
            //Check all existing circles and make sure there is no overlap:
            if(!checkOverlap(nx, ny, -1)) {
                coordinates.add(new Coordinates(nx, ny));
                return true;
            }
        }
        
        return false;
        
    }

    /**
     * Move all circles in a random direction.
     * @return 
     */
    private boolean optimizeSetStep() {
        
        //Generate step between -1 and 1 in x and y direction
        stepx = rand.nextInt(3)-1;
        stepy = rand.nextInt(3)-1;
        move = false;
        
        //Move through all circles:
        for(int i=0;i<coordinates.size();i++) {
            
            //Get current position:
            xc=(int)coordinates.get(i).x;
            yc=(int)coordinates.get(i).y;
            
            //Check if circle can be moved without getting out the box or overlapping another circle:
            if(yc+stepy >= (int)d/2 && xc+stepx >= (int)d/2 && yc+stepy <= (int)(this.y-(d/2)) && xc+stepx <= (int)(this.x-(d/2)) ) {            
                if(!checkOverlap(xc+stepx, yc+stepy, i)) {
                    coordinates.set(i, new Coordinates(xc+stepx,yc+stepy));
                    //return true;
                    move = true;
                }
            }
            
        }
        
        return move;
        
    }
    
}
