package optimization.Algorithms.helper;

/**
 * Class for the Array list. Holds Coordinates in mm for x and y of the circles.
 * @author Alexander von Birgelen
 */
public class Coordinates {
    public double x; //mm
    public double y; //mm
    
    /**
     * init coordinates
     * @param x x position in mm
     * @param y y position in mm
     */
    public Coordinates(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    /**
     * Print coordinates as String
     * @return 
     */
    @Override public String toString() {
        return "["+x+";"+y+"]";
    }
    
}
