package optimization.Algorithms.helper;

/**
 * Internal states of the Algorithms
 * @author Alexander von Birgelen
 */
public enum AlgState {
    INIT, //State after initializing
    RUNNING, //Running automatically
    PAUSED, //Paused
    STEP, //Stepping mode
    FINISHED; //Finished. No further operations needed/possible
}
